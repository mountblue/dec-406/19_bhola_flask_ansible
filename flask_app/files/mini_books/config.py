import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or \
        'ksg!&o0)erqi=bun9ta&(&+4l!^sc_c0nk=pv&x8_gh$!x*3z_'
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
        'mysql+pymysql://minibooksuser:mini@Books1@localhost/mini_books'

    SQLALCHEMY_TRACK_MODIFICATIONS = False
