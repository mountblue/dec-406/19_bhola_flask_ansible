from app import app


if __name__ == "__main__":
    app.run(host='0.0.0.0')

# from app import app, db
# from app.models import User, Role, Address, Client, Record


# @app.shell_context_processor
# def make_shell_context():
#     return {
#         'db': db,
#         'User': User,
#         'Role': Role,
#         'Address': Address,
#         'Client': Client,
#         'Record': Record
#     }
