import csv
from app import celery, APP_ROOT
from app.models import Client
import time
from app.elastic_util import store_record_client, connect_elasticsearch,\
    update_record_client


# celery task to make csv of a client
@celery.task(bind=True)
def export_client_task(self, client_id):
    client = Client.query.filter_by(id=client_id).first_or_404()
    self.update_state(state='PROGRESS',
                      meta={'current': 10, 'status': 'In progress...'})

    records = []
    records.append(["S.no", "Type", "Date", "Amount"])
    count = 1
    self.update_state(state='PROGRESS',
                      meta={'current': 20, 'status': 'In progress...'})
    for record in client.records:
        record_list = []
        record_list.append(count)
        record_list.append(record.type_)
        record_list.append(str(record.date_transaction.date()))
        record_list.append(record.amount)
        records.append(record_list)
        count += 1
    self.update_state(state='PROGRESS',
                      meta={'current': 50, 'status': 'In progress...'})
    print(records)
    csv.register_dialect('myDialect', quoting=csv.QUOTE_ALL,
                         skipinitialspace=True)

    timestamp = int(time.time())
    filepath = "/static/data/"+client.name + str(timestamp)+".csv"
    abs_filepath = APP_ROOT + filepath
    with open(abs_filepath, 'w') as file_csv:
        writer = csv.writer(file_csv, dialect='myDialect')
        for row in records:
            writer.writerow(row)
    self.update_state(state='PROGRESS',
                      meta={'current': 80, 'status': 'In progress...'})
    file_csv.close()
    return {
            'current': 100,
            'status': 'Task Completed',
            'filepath': filepath
        }


# client add to elastic celery task
@celery.task
def add_record_elastic(name):
    es_object = connect_elasticsearch()
    client = Client.query.filter_by(name=name).all()[-1]
    record = {"id": int(client.id), "name": client.name}
    store_record_client(es_object, record)


# client update to elastic celery task
@celery.task
def update_record_elastic(id, name):
    es_object = connect_elasticsearch()
    record = {"doc": {"name": name}}
    update_record_client(es_object, id, record)
