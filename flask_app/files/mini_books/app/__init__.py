from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
import logging
from logging.handlers import RotatingFileHandler
import os
from celery import Celery


app = Flask(__name__)
app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
login = LoginManager(app)

login.login_view = 'login'

# Celery configuration
app.config['CELERY_BROKER_URL'] =\
    os.environ.get('CELERY_BROKER_URL') or 'amqp://guest@localhost//'
app.config['CELERY_RESULT_BACKEND'] =\
    os.environ.get('CELERY_RESULT_BACKEND') or 'amqp://guest@localhost//'

# Initialize Celery
celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)

APP_ROOT = os.path.dirname(os.path.abspath(__file__))

from app import routes, models, errors, elastic_util

ES_ = elastic_util.connect_elasticsearch()
elastic_util.create_index_clients(ES_)

# elastic_util.store_record_client(ES_, body)

if not app.debug:
    # ...

    if not os.path.exists('logs'):
        os.mkdir('logs')
    file_handler = RotatingFileHandler('logs/mini_books.log', maxBytes=10240,
                                       backupCount=10)
    file_handler.setFormatter(logging.Formatter(
        '%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'))
    file_handler.setLevel(logging.INFO)
    app.logger.addHandler(file_handler)

    app.logger.setLevel(logging.INFO)
    app.logger.info('Mini Books startup')
