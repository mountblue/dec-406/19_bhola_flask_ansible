from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField,\
    SubmitField, SelectField, DecimalField, DateField
from wtforms.validators import ValidationError, DataRequired,\
    Email, EqualTo, Length
from app.models import User, Role, Client, Address, Record
import datetime


# Login Form
class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')


# Register Form
class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField(
        'Repeat Password', validators=[DataRequired(), EqualTo('password')])
    role_id = SelectField('Role', coerce=int, validators=[DataRequired()])
    submit = SubmitField('Add User')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user is not None:
            raise ValidationError('Please use a different username.')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('Please use a different email address.')

    def validate_role_id(self, role_id):
        role_id_check = Role.query.filter_by(id=role_id.data).first()
        if role_id_check is None:
            raise ValidationError('Please select correct role.')


# Client Add Form
class ClientForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    address_id =\
        SelectField('Address', coerce=int, validators=[DataRequired()])
    submit = SubmitField('Add Client')

    def validate_address_id(self, address_id):
        address_id_check = Address.query.filter_by(id=address_id.data).first()
        if address_id_check is None:
            raise ValidationError('Please select correct address.')


# Client Edit Form
class ClientEditForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    address_id =\
        SelectField('Address', coerce=int, validators=[DataRequired()])
    submit = SubmitField('Update Client')

    def validate_address_id(self, address_id):
        address_id_check = Address.query.filter_by(id=address_id.data).first()
        if address_id_check is None:
            raise ValidationError('Please select correct address.')


# Address Add Form
class AddressForm(FlaskForm):
    name = StringField('name', validators=[DataRequired()])
    submit = SubmitField('Add Location')

    def validate_name(self, name):
        address = Address.query.filter_by(name=name.data).first()
        if address is not None:
            raise ValidationError('Please use a different address.')


# Addredd Edit Form
class AddressEditForm(FlaskForm):
    name = StringField('name', validators=[DataRequired()])
    submit = SubmitField('Update Location')

    def validate_name(self, name):
        address = Address.query.filter_by(name=name.data).first()
        if address is not None:
            raise ValidationError('Please use a different address.')


# Record Form
class RecordForm(FlaskForm):
    client_name = StringField('Client Name', validators=[DataRequired()])
    type_ = StringField('type', validators=[DataRequired()])
    date_transaction =\
        DateField('Date of Transaction', validators=[DataRequired()],
                  format='%Y-%m-%d')
    amount = DecimalField('Amount', validators=[DataRequired()], places=2)
    submit = SubmitField('Add')

    def validate_client_name(self, client_name):
        name = Client.query.filter_by(name=client_name.data).first()
        if name is None:
            raise ValidationError('There is no client with this name.')

    def validate_type_(self, type_):
        if type_.data not in ['Sale', 'Deposit']:
            raise ValidationError('Something went wrong.')

    def validate_amount(self, amount):
        if amount.data <= 0:
            raise ValidationError('Amount must be greater than 0.')
